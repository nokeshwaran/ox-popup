<?php

namespace OX\NotificationPopup\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;

class ConfigOption implements OptionSourceInterface
{
    public function toOptionArray()
    {
        return [
            ['value' => '1', 'label' => __('Top Right')],
            ['value' => '2', 'label' => __('Top Left')],
            ['value' => '3', 'label' => __('Middle Right')],
            ['value' => '4', 'label' => __('Middle')],
            ['value' => '5', 'label' => __('Middle Left')],
            ['value' => '6', 'label' => __('Bottom Right')],
            ['value' => '7', 'label' => __('Bottom Left')]
        ];
    }
}
