<?php

namespace OX\NotificationPopup\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;

class ConfigDisplayPage implements OptionSourceInterface
{
    const ALL_PAGES = 1;
    const HOME_PAGE = 2;
    const CATALOG_PAGE = 3;
    const PRODUCT_DETAIL_PAGE = 4;
    const CART_PAGE = 5;
    const CHECKOUT_PAGE = 6;
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => self::ALL_PAGES, 'label' => __('All pages')],
            ['value' => self::HOME_PAGE, 'label' => __('Home page')],
            ['value' => self::CATALOG_PAGE, 'label' => __('Catalog page')],
            ['value' => self::PRODUCT_DETAIL_PAGE, 'label' => __('Product detail page')],
            ['value' => self::CART_PAGE, 'label' => __('Cart page')],
            ['value' => self::CHECKOUT_PAGE, 'label' => __('Checkout page')]
        ];
    }
}
