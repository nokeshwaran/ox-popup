<?php

namespace OX\NotificationPopup\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use OX\NotificationPopup\Helper\Data;
use Magento\Framework\App\Request\Http;
use OX\NotificationPopup\Model\Config\Source\ConfigDisplayPage;

class Popup extends Template
{
    private $helper;
    protected $_httpRequest;
    protected $contentProcessor;

    /**
     * Popup constructor.
     *
     * @param Context $context
     * @param Data $helper
     * @param Http $httpRequest
     */
    public function __construct(Context $context, Data $helper, Http $httpRequest)
    {
        parent::__construct($context);
        $this->helper = $helper;
        $this->_httpRequest = $httpRequest;
    }

    public function getconfigValue()
    {
        return $this->helper->getConfig('notification/general/cookies');
    }

    public function getstatus()
    {
        return $this->helper->getConfig('notification/general/enable');
    }

    public function getwidth()
    {
        return $this->helper->getConfig('notification/general/width');
    }

    public function getheight()
    {
        return $this->helper->getConfig('notification/general/height');
    }

    public function getposition()
    {
        return $this->helper->getConfig('notification/general/PopupPosition');
    }

    /**
     * Get pages to show Popup
     *
     * @return array|mixed
     */
    public function checkpagetoshow()
    {
        $pages = $this->helper->getConfig('notification/general/DisplayPage');
        $temp = explode(',', $pages);

        $tempvar = "";

        foreach ($temp as $page) {
            switch ($page) {
                case ConfigDisplayPage::ALL_PAGES:
                    return true;
                case ConfigDisplayPage::HOME_PAGE:
                    if ($this->getHandler() == "cms_index_index") {
                        $tempvar = true;
                    }
                    break;
                case ConfigDisplayPage::CATALOG_PAGE:
                    if ($this->getHandler() == "catalog_category_view") {
                        $tempvar = true;
                    }
                    break;
                case ConfigDisplayPage::PRODUCT_DETAIL_PAGE:
                    if ($this->getHandler() == "catalog_product_view") {
                        $tempvar = true;
                    }

                    break;
                case ConfigDisplayPage::CART_PAGE:
                    if ($this->getHandler() == "checkout_cart_index") {
                        $tempvar = true;
                    }
                    break;
                case ConfigDisplayPage::CHECKOUT_PAGE:
                    if ($this->getHandler() == "checkout_index_index") {
                        $tempvar = true;
                    }
                    break;
                case ConfigDisplayPage::SUCCESS_PAGE:
                    if ($this->getHandler() == "checkout_onepage_success") {
                        $tempvar = true;
                    }
                    break;

            }
        }

        return $tempvar;
    }

    /**
     * Get current page handler
     *
     * @return array|mixed
     */
    public function getHandler()
    {
        return $this->_httpRequest->getFullActionName();
    }
    public function getPopupImage()
    {
        return $this->helper->getConfig('notification/general/image_upload');
    }
    public function getHeading()
    {
        return $this->helper->getConfig('notification/general/title');
    }
    public function getContent()
    {
        return $this->helper->getConfig('notification/general/content');
    }
}
