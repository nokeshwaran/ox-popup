define([
    'jquery',
    'Magento_Ui/js/modal/modal',
    'mage/cookies',
    'matchMedia'
], function (
    $,
    modal
) {
    'use strict';
    return function (config, element) {
        var myData = config.customData;
        var userwidth = config.customwidth;
        var userheight = config.customheight;
        var userposition = config.customposition;
        var seconds = myData * 1000;
        setTimeout(function () {
            var modaloption = {
                type: 'popup',
                autoOpen: false,
                modalClass: 'notification-popup',
                responsive: true,
                innerScroll: true,
                clickableOverlay: true,
                heightStyle: "content",
                buttons: false,
                fixedPopup: true,
            };


                modal(modaloption, $('#notification-modal'));


            $('.notification-popup').find('.modal-inner-wrap').addClass('notification-width');
            $('.notification-width').css("width", userwidth + 'px');
            $('.notification-popup').find('.modal-inner-wrap').addClass('notification-height');
            $('.notification-height').css("height", userheight + 'px');

            if (userposition == 1) {
                $('.notification-popup').find('.modal-inner-wrap').css({
                    "top": "0",
                    "left": "auto",
                    "transform": "translate(0%,calc(0% + 60px))"
                });
            } else if (userposition == 2) {
                $('.notification-popup').find('.modal-inner-wrap').css({
                    "top": "0",
                    "right": "-100%",
                    "position": "absolute",
                    "transform": "translate(0%,calc(0% + 60px))"
                });
            } else if (userposition == 3) {
                $('.notification-popup').find('.modal-inner-wrap').css({
                    "transform": "translate(-50%, -100%)",
                    "left": "90%"
                });
            } else if (userposition == 4) {
                $('.notification-popup').find('.modal-inner-wrap').css({
                    "transform": "translate(-50%, -100%)",
                    "top": "50%",
                    "left": "50%"
                });
            } else if (userposition == 5) {
                $('.notification-popup').find('.modal-inner-wrap').css({
                    "transform": "translate(0, -100%)",
                    "top": "50%;"
                });
            } else if (userposition == 6) {
                $('.notification-popup').find('.modal-inner-wrap').css({
                    "bottom": "0",
                    "left": "auto",
                    "right": "0"
                });
            } else if (userposition == 7) {
                $('.notification-popup').find('.modal-inner-wrap').css({
                    "bottom": "0",
                    "left": "0"
                });
            }

            if ($.cookie('popupShown') != 1) {

                $("#notification-modal").modal("openModal");

            }
            $('#notification-modal').on('modalclosed', function () {
                $.cookie('popupShown', 1, {path: '/'})
            });
        }, seconds);
    }
});

